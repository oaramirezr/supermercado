from typing import Optional
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from uuid import uuid4 as uuid

router = FastAPI()

lista = []
# Modelo de estructura
class Modelo(BaseModel):
    id: Optional[str]
    titulo: str
    contenido: Optional[str]

# CRUD
@router.get("/saludo/{nombre}")
def saludo(nombre:str):
    salida = (f"¡Bienvenido {nombre}!")
    return salida


@router.get("/get")
def get_model():
    return lista

@router.post("/crear")
def guardar_contenido(crear: Modelo):
    crear.id = str(uuid())
    lista.append(crear.dict())
    print(crear.dict())
    return lista[-1]

@router.get("/get_by_id/{id}")
def get_by_id(id:str):
    for list in lista:
        if list["id"]== id:
            return list
    raise HTTPException(status_code=404, detail="No encontrado")

@router.delete("/delete_by_id/{id}")
def delete_by_id(id:str):
    for index, list in enumerate(lista):
        if list["id"]== id:
            lista.pop(index)
            return "Se ha eliminado satisfactoriamente"
    raise HTTPException(status_code=404, detail="No encontrado")

@router.put("/put_by_id/{id}")
def put_by_id(id:str, updateList: Modelo):
    for index, list in enumerate(lista):
        if list["id"]== id:
            lista[index]["titulo"]= updateList.titulo
            lista[index]["contenido"]= updateList.contenido
            return "Se ha actualizado satisfactoriamente"
    raise HTTPException(status_code=404, detail="No encontrado")

# - Instalar fastapi
#     pip install fastapi
# - Instalar uvicorn 
#     pip install uvicorn
# - Ver paquetes instalados de pip 
#     pip freeze
# - Escuchar el archivo 
#     uvicorn crud_routers_examples:router --reload

